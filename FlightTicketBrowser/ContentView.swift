//
//  ContentView.swift
//  FlightTicketBrowser
//
//  Created by Jyoti Bindu on 29/06/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TicketListView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
