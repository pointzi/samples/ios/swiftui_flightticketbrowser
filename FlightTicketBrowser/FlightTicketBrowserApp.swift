//
//  FlightTicketBrowserApp.swift
//  FlightTicketBrowser
//
//  Created by Takuya Aso on 2021/12/22.
//

import SwiftUI
import UIKit
@main
struct FlightTicketBrowserApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {
        WindowGroup {
            TicketListView()
            
        }
    }
    
 }

 

