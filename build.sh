#!/bin/sh -x

echo "===== FlightTicketBrowser Build started on $(date) ====="
rm -Rf ./build/outputs/*
ARTIFAC_USER="ganesh"

echo "===== Record git branch and commit hash ====="

GIT_VERSION=$(git log -1 --format="%h")
if [ -z "$CI_COMMIT_REF_NAME" ]; then
    GIT_BRANCH=$(git symbolic-ref --short -q HEAD)
else
    GIT_BRANCH=$CI_COMMIT_REF_NAME
fi
BUILD_TIME=$(date)

/usr/libexec/PlistBuddy -c "Set :CFBundleVersion '${GIT_BRANCH}-${GIT_VERSION}-${BUILD_TIME}'" "./FlightTicketBrowser/Info.plist"


# ------------------- FlightTicketBrowser ------------------------
echo "===== Build FlightTicketBrowser app ====="

bundle install
bundle update fastlane
pod update
bundle exec fastlane beta_release
xcodebuild -workspace FlightTicketBrowser.xcworkspace -scheme FlightTicketBrowser clean
xcodebuild -quiet -workspace FlightTicketBrowser.xcworkspace -scheme FlightTicketBrowser -destination 'generic/platform=iOS Simulator' -configuration Debug -archivePath build/FlightTicketBrowser.xcarchive archive

echo "===== Distribute FlightTicketBrowser app ====="
rm -Rf build/FlightTicketBrowser.app
mv build/FlightTicketBrowser.xcarchive/Products/Applications/FlightTicketBrowser.app build/FlightTicketBrowser.app
zip -r build/FlightTicketBrowser.zip build/FlightTicketBrowser.app

echo "===== Uploading .app zip to Artifactory ====="
/usr/local/opt/curl/bin/curl --user $ARTIFAC_USER:$ARTIFACTORY_PASSWORD -T build/FlightTicketBrowser.zip -X PUT \
    "https://repo.pointzi.com/sdk/pointzi/ios/bdd/dev_bison/SwiftUI_FlightTicket/FlightTicketBrowser.zip"

echo "===== Uploading .ipa to AppCenter ====="
appcenter distribute release --app Contextual/FlightTicketBrowser-DevSDK-Prod-SwiftUI_FlightTicket --file FlightTicketBrowser.ipa --group "Collaborators"
